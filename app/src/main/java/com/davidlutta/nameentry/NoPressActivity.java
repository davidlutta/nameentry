package com.davidlutta.nameentry;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class NoPressActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_press);
    }
}