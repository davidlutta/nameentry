package com.davidlutta.nameentry;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    public static final String NAME_EXTRA = "name";
    private EditText mPersonNameEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPersonNameEditText = (EditText) findViewById(R.id.editTextTextPersonName);
    }
    public void pressMe(View view) {
        String name = mPersonNameEditText.getText().toString();
        Intent intent = new Intent(MainActivity.this, NameActivity.class);
        intent.putExtra(NAME_EXTRA, name);
        startActivity(intent);
    }

    public void noPress(View view) {
        startActivity(new Intent(MainActivity.this, NoPressActivity.class));
    }
}