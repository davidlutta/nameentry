package com.davidlutta.nameentry;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class NameActivity extends AppCompatActivity {
    private TextView mNameTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        mNameTxtView = (TextView) findViewById(R.id.nameTextView);
        setName();
    }

    private void setName() {
        if (getIntent().hasExtra(MainActivity.NAME_EXTRA)) {
            String name = getIntent().getStringExtra(MainActivity.NAME_EXTRA);
            mNameTxtView.setText(String.format("Bonjuor %s", name));
        }
    }
}